//
//  Users.swift
//  Fire chat
//
//  Created by Victor Kachalov on 10.09.17.
//  Copyright © 2017 vk. All rights reserved.
//

import Foundation

struct Users {
    var id   : String
    var name : String
}
