//
//  ChatViewController.swift
//  Fire chat
//
//  Created by Victor Kachalov on 10.09.17.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage
import JSQMessagesViewController

class ChatViewController: JSQMessagesViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

//    var imagePickerController: UIImagePickerController!
//    var imageTake: UIImageView!
    
    
    // === зададим 2х стандартных пользователя
    let user1 = Users.init(id: "1", name: "A")
    let user2 = Users.init(id: "2", name: "B")
    
    // === установим текущего пользователя (исходящие сообщения)
    var currentUser : Users { return user1 }
    
    // === будем собирать все сообщения в массив
    var messages = [JSQMessage]()
    
    // === Застолбим переменную для связи с БД
    var messagesRef: DatabaseReference?
    var storageRef: StorageReference?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //=== обязательно сообщаем JSQMessagesViewController,
        //=== кто является текущим пользователем
        self.senderId = currentUser.id
        self.senderDisplayName = currentUser.name
        
        messages = getMessages()
        
        // === Установим титул на нав баре
        self.navigationItem.title = "Simple chat"
        
        // === Добавим кнопку на навбар с целью копирования нашего сообщения
        // === в качестве ответа собеседника
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage.jsq_defaultTypingIndicator(), style: .plain, target: self, action: #selector(receiveMessagePressed))
        
        
    }
    
    //НАЧАЛО : === РЕАЛИЗОВЫВАЕМ ОБЯЗАТЕЛЬНЫЕ МЕТОДЫ JSQMessagesViewController ===
    
    //--- начало: возвращаем количество сообщений
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }//--- конец: возвращаем количество сообщений
    
    //--- начало: возвращаем содержимое сообщений
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.row]
    }//--- конец: возвращаем содержимое сообщений
    
    //--- начало: задаем цвета сообщений в диалоге
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let bubleFactory = JSQMessagesBubbleImageFactory()
        let message = messages[indexPath.row]
        if currentUser.id == message.senderId {
            return bubleFactory?.outgoingMessagesBubbleImage(with: UIColor.green)
        } else {
            return bubleFactory?.incomingMessagesBubbleImage(with: UIColor.blue)
        }
    }//--- конец: задаем цвета сообщений в диалоге
    
    //--- начало: устанавливаем аватарки
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil //решил без аватарок
    }//--- конец: устанавливаем аватарки
    
    //--- начало: устанавливаем подписи к сообщения (от кого:)
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.row]
        let messageUsername = message.senderDisplayName
        return NSAttributedString(string: messageUsername!)
    }//--- конец: устанавливаем подписи к сообщения (от кого:)
    
    //--- начало: устанавливаем высоту "облачков" сообщений
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }//--- конец: устанавливаем высоту "облачков" сообщений
    
    //--- начало: отправка исходящих сообщений
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let message = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text)
        messages.append(message!)
        finishSendingMessage()
        
        // --- начало: добавляем исходящие сообщения в базу ==
        messagesRef = Database.database().reference()
        messagesRef?.child("Messages").childByAutoId().setValue([
            "text"       :message?.text,
            "senderId"   :message?.senderId,
            "senderName" :message?.senderDisplayName])
    
        // --- конец: добавляем исходящие сообщения в базу ===
        
    }//--- конец: отправка исходящих сообщений
    
    
    //КОНЕЦ : === РЕАЛИЗОВЫВАЕМ ОБЯЗАТЕЛЬНЫЕ МЕТОДЫ JSQMessagesViewController ===
    
    
    
    
    //НАЧАЛО : === РЕАЛИЗОВЫВАЕМ МЕТОДЫ JSQMessagesViewController ДЛЯ РАБОТЫ С ПРИКРПЕЛЕННЫМИ ФАЙЛАМИ ===
    
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        
        let sheet = UIAlertController(title: "Media messages", message: nil, preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Send photo", style: .default) { (action) in
            /**
             *  Create fake photo
             */
            
            self.importImage()
            

            
        }
        
        let locationAction = UIAlertAction(title: "Send location", style: .default) { (action) in
            /**
             *  Add fake location
             */
            let locationItem = self.buildLocationItem()
            
            self.addMedia(locationItem)
        }
        
        let videoAction = UIAlertAction(title: "Send video", style: .default) { (action) in
            /**
             *  Add fake video
             */
            let videoItem = self.buildVideoItem()
            
            self.addMedia(videoItem)
        }
        
        let audioAction = UIAlertAction(title: "Send audio", style: .default) { (action) in
            /**
             *  Add fake audio
             */
            let audioItem = self.buildAudioItem()
            
            self.addMedia(audioItem)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        sheet.addAction(photoAction)
        sheet.addAction(locationAction)
        sheet.addAction(videoAction)
        sheet.addAction(audioAction)
        sheet.addAction(cancelAction)
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func buildVideoItem() -> JSQVideoMediaItem {
        let videoURL = URL(fileURLWithPath: "file://")
        
        let videoItem = JSQVideoMediaItem(fileURL: videoURL, isReadyToPlay: true)
        
        return videoItem!
    }
    
    func buildAudioItem() -> JSQAudioMediaItem {
        let sample = Bundle.main.path(forResource: "jsq_messages_sample", ofType: "m4a")
        let audioData = try? Data(contentsOf: URL(fileURLWithPath: sample!))
        
        let audioItem = JSQAudioMediaItem(data: audioData)
        
        return audioItem
    }
    
    func buildLocationItem() -> JSQLocationMediaItem {
        let ferryBuildingInSF = CLLocation(latitude: 37.795313, longitude: -122.393757)
        
        let locationItem = JSQLocationMediaItem()
        locationItem.setLocation(ferryBuildingInSF) {
            self.collectionView!.reloadData()
        }
        
        return locationItem
    }
    
    //MARK: Photo attaching
    
    func importImage () { // --- start: достаем фотку из фотоЛибрари
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePickerController.allowsEditing = false
        self.present(imagePickerController, animated: true, completion: nil)
        
    } // --- end: достаем фотку из фотоЛибрари
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) { // --- start: если удачно достали фото из фотоЛибрари, то добавим к сообщениям
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let photoItem = JSQPhotoMediaItem(image: image)
            self.addMedia(photoItem!)
            
            // --- сохраняем исходящую картинку в хранилище Firebase Storage
            self.storageRef = Storage.storage().reference().child("OutcomingImage").child(String(Int(arc4random())))
            if let uploadData = UIImagePNGRepresentation((photoItem?.image)!) {
                storageRef?.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                    if error != nil {
                        print(error)
                        return
                    }
                })
            }// end: --- сохраняем картинку в хранилище Firebase Storage
        } else {
            print ("There was a problem with the image")
        }
        self.dismiss(animated: true, completion: nil)
    } // --- end: если удачно достали фото из фотоЛибрари, то добавим к сообщениям

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addMedia(_ media:JSQMediaItem) {
        let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: media)
        self.messages.append(message!)
        
        //Optional: play sent sound
        
        self.finishSendingMessage(animated: true)
    }
    
    
    //КОНЕЦ : === РЕАЛИЗОВЫВАЕМ МЕТОДЫ JSQMessagesViewController ДЛЯ РАБОТЫ С МЕДИА И ЛОКАЦИЕЙ===
    
    
    
    
    //--- начало: ставим начальные сообщения
    func getMessages() -> [JSQMessage] {
        var messages = [JSQMessage]()
        var message1 = JSQMessage(senderId: user1.id, displayName: user1.name, text: "Hello, how are you?")
        var message2 = JSQMessage(senderId: user2.id, displayName: user2.id, text: "Fine, thanks! And you?")
        messages.append(message1!)
        messages.append(message2!)
        return messages
    }//--- конец: ставим начальные сообщения
    
    
    //--- начало: делаем сообщения собеседника (копированием наших)
    func receiveMessagePressed(_ sender: UIBarButtonItem) {
        /**
         *  DEMO ONLY
         *
         *  The following is simply to simulate received messages for the demo.
         *  Do not actually do this.
         */
        
        /**
         *  Show the typing indicator to be shown
         */
        self.showTypingIndicator = !self.showTypingIndicator
        
        /**
         *  Scroll to actually view the indicator
         */
        self.scrollToBottom(animated: true)
        
        /**
         *  Copy last sent message, this will be the new "received" message
         */
        var copyMessage = self.messages.last?.copy()
        
        if (copyMessage == nil) {
            copyMessage = JSQMessage(senderId: user2.id, displayName: user2.name, text: "First received!")
        }
        
        var newMessage:JSQMessage!
        var newMediaData:JSQMessageMediaData!
        var newMediaAttachmentCopy:AnyObject?
        
        if (copyMessage! as AnyObject).isMediaMessage() {
            /**
             *  Last message was a media message
             */
            let copyMediaData = (copyMessage! as AnyObject).media
            
            switch copyMediaData {
            case is JSQPhotoMediaItem:
                let photoItemCopy = (copyMediaData as! JSQPhotoMediaItem).copy() as! JSQPhotoMediaItem
                photoItemCopy.appliesMediaViewMaskAsOutgoing = false
                
                newMediaAttachmentCopy = UIImage(cgImage: photoItemCopy.image!.cgImage!)
                
                /**
                 *  Set image to nil to simulate "downloading" the image
                 *  and show the placeholder view5017
                 */
                photoItemCopy.image = nil;
                
                newMediaData = photoItemCopy
            case is JSQLocationMediaItem:
                let locationItemCopy = (copyMediaData as! JSQLocationMediaItem).copy() as! JSQLocationMediaItem
                locationItemCopy.appliesMediaViewMaskAsOutgoing = false
                newMediaAttachmentCopy = locationItemCopy.location!.copy() as AnyObject?
                
                /**
                 *  Set location to nil to simulate "downloading" the location data
                 */
                locationItemCopy.location = nil;
                
                newMediaData = locationItemCopy;
            case is JSQVideoMediaItem:
                let videoItemCopy = (copyMediaData as! JSQVideoMediaItem).copy() as! JSQVideoMediaItem
                videoItemCopy.appliesMediaViewMaskAsOutgoing = false
                newMediaAttachmentCopy = (videoItemCopy.fileURL! as NSURL).copy() as AnyObject?
                
                /**
                 *  Reset video item to simulate "downloading" the video
                 */
                videoItemCopy.fileURL = nil;
                videoItemCopy.isReadyToPlay = false;
                
                newMediaData = videoItemCopy;
            case is JSQAudioMediaItem:
                let audioItemCopy = (copyMediaData as! JSQAudioMediaItem).copy() as! JSQAudioMediaItem
                audioItemCopy.appliesMediaViewMaskAsOutgoing = false
                newMediaAttachmentCopy = (audioItemCopy.audioData! as NSData).copy() as AnyObject?
                
                /**
                 *  Reset audio item to simulate "downloading" the audio
                 */
                audioItemCopy.audioData = nil;
                
                newMediaData = audioItemCopy;
            default:
                assertionFailure("Error: This Media type was not recognised")
            }
            
            newMessage = JSQMessage(senderId: user2.id, displayName: user2.name, media: newMediaData)
        }
        else {
            /**
             *  Last message was a text message
             */
            
            newMessage = JSQMessage(senderId: user2.id, displayName: user2.name, text: (copyMessage! as AnyObject).text)
        }
        
        /**
         *  Upon receiving a message, you should:
         *
         *  1. Play sound (optional)
         *  2. Add new JSQMessageData object to your data source
         *  3. Call `finishReceivingMessage`
         */
        
        self.messages.append(newMessage)
        self.finishReceivingMessage(animated: true)
        
        if newMessage.isMediaMessage {
            /**
             *  Simulate "downloading" media
             */
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                /**
                 *  Media is "finished downloading", re-display visible cells
                 *
                 *  If media cell is not visible, the next time it is dequeued the view controller will display its new attachment data
                 *
                 *  Reload the specific item, or simply call `reloadData`
                 */
                
                switch newMediaData {
                case is JSQPhotoMediaItem:
                    (newMediaData as! JSQPhotoMediaItem).image = newMediaAttachmentCopy as? UIImage
                    self.collectionView!.reloadData()
                case is JSQLocationMediaItem:
                    (newMediaData as! JSQLocationMediaItem).setLocation(newMediaAttachmentCopy as? CLLocation, withCompletionHandler: {
                        self.collectionView!.reloadData()
                    })
                case is JSQVideoMediaItem:
                    (newMediaData as! JSQVideoMediaItem).fileURL = newMediaAttachmentCopy as? URL
                    (newMediaData as! JSQVideoMediaItem).isReadyToPlay = true
                    self.collectionView!.reloadData()
                case is JSQAudioMediaItem:
                    (newMediaData as! JSQAudioMediaItem).audioData = newMediaAttachmentCopy as? Data
                    self.collectionView!.reloadData()
                default:
                    assertionFailure("Error: This Media type was not recognised")
                }
            }
        }
        
        // --- начало: добавляем входящие сообщения в базу ==
        messagesRef = Database.database().reference()
        messagesRef?.child("Messages").childByAutoId().setValue([
            "text"       :newMessage.text,
            "senderId"   :newMessage.senderId,
            "senderName" :newMessage.senderDisplayName])
        // --- конец: добавляем входящие сообщения в базу ===
        
        // --- сохраняем входящую картинку в хранилище Firebase Storage
        self.storageRef = Storage.storage().reference().child("IncomingImage").child(String(Int(arc4random())))
        if let uploadData = UIImagePNGRepresentation(newMediaAttachmentCopy as! UIImage) {
            storageRef?.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                if error != nil {
                    print(error)
                    return
                }
            })
        } // end: --- сохраняем входящую картинку в хранилище Firebase Storage
        
        
    }//--- конец: делаем сообщения собеседника (копированием наших)


}
